var util = require('../../utils/util.js');
var api = require('../../config/api1.js');

Page({
  data: {
    navList: [], 
    // categoryList: [],
    currentCategory: {},
    scrollLeft: 0,
    scrollTop: 0,
    goodsCount: 0,
    scrollHeight: 0
  },
  onLoad: function (options) {
    this.getCatalog();
  },
  getCatalog: function () {
    //CatalogList
    let that = this;
    wx.showLoading({
      title: '加载中...',
    });
    // 导航数据
    util.request(api.CatalogList).then(function (res) {
      // console.log(res.data)
        that.setData({
          navList: res.data.categoryList,
          currentCategory: res.data.currentCategory
        });
        wx.hideLoading();
      });
      // 商品总数
    util.request(api.GoodsCount).then(function (res) {
      // console.log(res.data)
      that.setData({
        goodsCount: res.data.goodsCount
      });
    });

  },
  // 获取目录当前的分类数据
  getCurrentCategory: function (id) {
    let that = this;
    util.request(api.CatalogCurrent, { id: id }, "GET")
      .then(function (res) {
        console.log(res.data.currentCategory)
        that.setData({
          currentCategory: res.data.currentCategory
        });
      });
    // wx.request({
    //   url: api.CatalogCurrent,
    //   data:{
    //     id:id
    //   },
    //   success:(res)=>{
    //     console.log(res.data.data.currentCategory)
    //   }
    // })
  },
  onReady: function () {
    // 页面渲染完成
  },
  onShow: function () {
    // 页面显示
  },
  onHide: function () {
    // 页面隐藏
  },
  onUnload: function () {
    // 页面关闭
  },
  // getList: function () {
  //   var that = this;
  //   util.request(api.ApiRootUrl + 'api/catalog/' + that.data.currentCategory.cat_id)
  //     .then(function (res) {
  //       that.setData({
  //         categoryList: res.data,
  //       });
  //     });
  // },
  switchCate: function (event) {
    let { id = 1005000,index=0} = event.currentTarget.dataset
    console.log(id)
    if (this.data.currentCategory.id === id) {
      return false; //判断点击相同的选项之后，不再请求数据
    }

    this.getCurrentCategory(id);
  }
})