const util = require('../../utils/util.js');
const api = require('../../config/api1.js');
// const user = require('../../services/user.js');

//获取应用实例
const app = getApp() 
Page({
  data: {
    newGoods: [],
    hotGoods: [],
    topics: [],
    brand: [],
    floorGoods: [],
    banner: [],
    channel: [],
    isScroll:true
  },
  onShareAppMessage: function () {
    return {
      title: 'NideShop',
      desc: '仿网易严选微信小程序商城',
      path: '/pages/index/index'
    }
  },onPullDownRefresh(){
	  	// 增加下拉刷新数据的功能
	    var self = this;
	    this.getIndexData();
 },
  getIndexData: function () {
    let that = this;
    var data = new Object();
 
    util.request(api.IndexUrl).then((res)=>{
      console.log(res.data)
      this.setData({
        // 轮播图
        banner:res.data.banner,
        // channel
        channel:res.data.channel,
        // brand
        brand: res.data.brandList,
        // topic
        topics: res.data.topicList,
        // newGoods
        newGoods: res.data.newGoodsList,
        // hotGoods
        hotGoods: res.data.hotGoodsList
      })
    });
   
   
  },
  onLoad: function (options) {
    this.getIndexData();
  },
  onReady: function () {
    // 页面渲染完成
  },
  onShow: function () {
    // 页面显示
  },
  onHide: function () {
    // 页面隐藏
  },
  onUnload: function () {
    // 页面关闭
  },
})
