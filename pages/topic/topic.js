var util = require('../../utils/util.js');
var api = require('../../config/api1.js');
var app = getApp()
Page({
    data: {
        // text:"这是一个页面"
        topicList: [], 
      currentPage: 1,
        size: 10,
        count: 0,
        scrollTop: 0,
        showPage: false
    },
    onLoad: function (options) {
        // 页面初始化 options为页面跳转所带来的参数
        this.getTopic();
    },
    onReady: function () {
        // 页面渲染完成
    },
    onShow: function () {
        // 页面显示
      // this.getTopic();
    },
    onHide: function () {
        // 页面隐藏
    },
    onUnload: function () {
        // 页面关闭
    },
    nextPage: function (event) {
      console.log(111)
        var that = this;
      // 直接引用的方式
      // if (this.data.page + 1 > that.data.count / that.data.size) {
      //       return true; //只有一页
      //   }
        // that.setData({
        //   currentPage: parseInt(that.data.currentPage) + 1
        // });

    //  封装函数的方式
      let { currentPage, size, count} = that.data
      let page = util.nextPage(currentPage, size, count)
      that.setData({
        currentPage:page
      })
        this.getTopic();  
    },
    getTopic: function(){
       
        let that = this;
         that.setData({
            scrollTop: 0,
            showPage: false,
            topicList: []
        });
        // 页面渲染完成
        wx.showToast({
            title: '加载中...',
            icon: 'loading',
            duration: 2000
        });

      // util.request(api.TopicList, { page: that.data.currentPage, pageSize: that.data.size }).then(function (res) {
      //   console.log(res.data)
      //     if (res.errno === 0) {
      //       that.setData({
      //         scrollTop: 0,
      //         topicList: res.data.data,
      //         showPage: true,
      //         count: res.data.data.count
      //       });
      //     }
      //     wx.hideToast();
      //   });
        wx.request({
          url: api.TopicList,
          data:{
            page:that.data.currentPage,
            pageSize:that.data.size
          },
          success:(res)=>{
            console.log(res.data)
            that.setData({
              scrollTop: 0,
              topicList: res.data.data.data,
              showPage: true,
              count: res.data.data.count
            })
            wx.hideToast();
          }
        })
    },
    prevPage: function (event) {
      //直接引用的方式
      // if (this.data.currentPage <= 1) {
      //       return false;
      //   }
      //   var that = this;
      //   that.setData({
      //     currentPage: parseInt(that.data.currentPage) - 1
      //   });

      // 引用封装函数
      let {currentPage} = this.data
      let page = util.prevPage(currentPage)
      this.setData({
        currentPage:page
      })
        this.getTopic();
    }
})